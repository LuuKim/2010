import java.util.List;

public class CompanyNode implements Comparable<CompanyNode> {
    private Integer money;
    private BinarySearchTree<CompanyNode> childs;
    public CompanyNode worstChild;

    // TODO: initialisation
    // O(1)
    public CompanyNode(Integer data) {
    	money = data;
    	childs = null;
    	worstChild = null;
    }

    // TODO: la compagnie courante achete une autre compagnie
    // O(log(n))
    public void buy(CompanyNode item) {
    	
    	if (childs != null)
    	{
	    	if(worstChild != null)
	    	{
	    		if(item.worstChild != null) {
			    	if (item.compareTo(item.worstChild) > 0) 
			    	{
			    		worstChild = item.worstChild;
			    		childs.insert(item);
			    	}
			    	else if( worstChild.compareTo(item.worstChild) > 0 && item.compareTo(item.worstChild) < 0)
			    	{
			    		worstChild = item;
			    		childs.insert(item);
			    	}
	    		}
	    		else //item.childs == null
	    		{
	    			if(worstChild.compareTo(item) > 0)
	    			{
	    				worstChild = item;
	    				childs.insert(item);
	    			}
	    			else
	    			{
	    				childs.insert(item);
	    			}
	    		}
	    	}
    	}
    	else
    	{
    		childs = new BinarySearchTree<CompanyNode>(item);
    		if(item.worstChild != null && item.compareTo(item.worstChild) > 0)
    		{

	    		worstChild = item.worstChild;
	    	}
	    	else
    		{
    			worstChild = item;
    		}
    	}
    	money += item.getMoney();
    }

    // TODO: on retourne le montant en banque de la compagnie
    // O(1)
    public Integer getMoney() {
        return money;
    }

    // TODO: on rempli le builder de la compagnie et de ses enfants avec le format
    //A
    // > A1
    // > A2
    // >  > A21...
    // les enfants sont afficher du plus grand au plus petit (voir TestCompany.testPrint)
    // O(n)
    public void fillStringBuilderInOrder(StringBuilder builder, String prefix) {
    	
    	builder.append(prefix + this.getMoney() + '\n');
    	if(childs != null) 
    	{
	    	prefix += " > ";
	    	List<BinaryNode<CompanyNode>> ordre = childs.getItemsInOrder();
	    	for (int i = ordre.size()-1 ; i >= 0 ; i-- )
    		{
	    		ordre.get(i).getData().fillStringBuilderInOrder(builder, prefix);
    		}
    	}
    }

    // TODO: on override le comparateur pour defenir l'ordre
    @Override
    public int compareTo(CompanyNode item) {
	   return (money.intValue() - item.getMoney().intValue());
    }
}
