import java.util.List;

public class BinaryNode<T extends Comparable<? super T> > {
    private T data;
    private BinaryNode<T> right;
    private BinaryNode<T> left;

    // TODO: initialisation
    // O(1)
    public BinaryNode(T data) {
    	this.data = data;
    	right = left = null;
    }

    // TODO: on retourne la donnee voulue
    // O(1)
    public T getData() {

    	return data;
    }

    // TODO: on ajoute une nouvelle donnee au bon endroit
    // O(log(n))
    public void insert(T item) {
    	
    	if(data != null)
    	{
			if(item.compareTo(data) <= 0) // si item <= data
			{
				if(left != null)
				{
					left.insert(item);
				}
				else
				{
					left = new BinaryNode<T>(item);
				}
			}
			else // si item > data
			{
				if(right != null)
				{
					right.insert(item);
				}
				else
				{
					right = new BinaryNode<T>(item);

				}
			}
		}
    	else 
    	{
    		data = item;
    	}
    }


    // TODO: est-ce que l'item fais partie du noeuds courant
    // O(log(n))
    public boolean contains(T item) {
    	if(data.equals(item))
    	{
    		return true;
    	}
		if(item.compareTo(data) <= 0) // si item <= data
		{
			if(left != null)
			{
				return(left.contains(item));
			}
			else
			{
				return false;
			}
		}
		else // si item > data
		{
			if(right != null)
			{
				return(right.contains(item));
			}
			else
			{
				return false;
			}
		}
    }

    // TODO: on retourne la maximale de l'arbre
    // O(n)
    public int getHeight() {
    	
    	if(data != null)
    	{
    		if(left != null && right != null)
			{
    			return Math.max(left.getHeight() + 1, right.getHeight() + 1);
			}
    		else if(left != null && right == null)
    		{
    			return left.getHeight() + 1;
    		}
    		else if(left == null && right != null)
    		{
    			return right.getHeight() + 1;
    		}		
    	}
    	return 0;
    	
    }
    // TODO: l'ordre d'insertion dans la liste est l'ordre logique
    // de manière que le plus petit item sera le premier inseré
    // O(n)
    public void fillListInOrder(List<BinaryNode<T>> result) {
    	if(data != null)
    	{
    		if(left != null)
    		{
    			left.fillListInOrder(result);
    		}

			result.add(this);
			
            if(right != null)
    		{
    			right.fillListInOrder(result);
    		}
    	}
    }
}

