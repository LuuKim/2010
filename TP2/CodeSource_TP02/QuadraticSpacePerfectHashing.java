import java.util.ArrayList;
import java.util.Random;

public class QuadraticSpacePerfectHashing<AnyType> 
{
	static int p = 46337;

	int a, b;
	AnyType[] items;

	QuadraticSpacePerfectHashing()
	{
		a=b=0; items = null;
	}

	QuadraticSpacePerfectHashing(ArrayList<AnyType> array)
	{
		AllocateMemory(array);
	}

	public void SetArray(ArrayList<AnyType> array)
	{
		AllocateMemory(array);
	}

	public int Size()
	{
		if( items == null ) return 0;

		return items.length;
	}

	public boolean containsKey(int key)
	{
		// A completer
		return !(items[key]==null);
	}

	public boolean containsValue(AnyType x )
	{
		// A completer
		if(Size() != 0)
		{
			if (items[getKey(x)] != null && items[getKey(x)].equals(x))
			{
				return true;
			}
		}
		return false;
	}

	public void remove (AnyType x) {
		// A completer
		if(containsValue(x))
		{
			items[getKey(x)] = null;
		}
	}

	public int getKey (AnyType x) {
		// A completer
		return (((a*x.hashCode()+b)%p)%items.length);

	}

	@SuppressWarnings("unchecked")
	private void AllocateMemory(ArrayList<AnyType> array)
	{
		Random generator = new Random( System.nanoTime() );
						
		if(array == null || array.size() == 0)
		{
			// A completer
			return;
		}
		if(array.size() == 1)
		{
			a = b = 0;

			// A completer
			items = (AnyType []) new Object[1];
			items[0] = array.get(0);
			return;
		}
		// A completer
		
		boolean collision = false;
		do {
			collision = false;
			a = generator.nextInt(p-1)+1;
			b = generator.nextInt(p);
			
			items = (AnyType []) new Object[array.size()*array.size()];
			
			for(int i = 0; i < array.size(); i++)
			{
				if (containsKey(getKey(array.get(i))))
				{
					collision = true;
				}
				else
				{
					items[getKey(array.get(i))] = array.get(i);
				}
			}
		} while (collision);
		
		return;
	}
	
	public String toString () {
		String result = "";
		
		// A completer
		
		for(int i = 0; i < Size(); i++)
		{ 
			if(items[i] != null)
			{
				result += "(" + getKey(items[i]) + "," + items[i] + "),"; 
			}
		}
		result = result.substring(0,result.length()-1) + ".";

		return result;
	}

	public void makeEmpty () {
		   // A completer
		items = null;   	
	}
}
