import java.util.Random;
import java.util.ArrayList;

public class LinearSpacePerfectHashing<AnyType>
{
	static int p = 46337;

	QuadraticSpacePerfectHashing<AnyType>[] data;
	int a, b;

	LinearSpacePerfectHashing()
	{
		a=b=0; data = null;
	}

	LinearSpacePerfectHashing(ArrayList<AnyType> array)
	{
		AllocateMemory(array);
	}

	public void SetArray(ArrayList<AnyType> array)
	{
		AllocateMemory(array);
	}

	@SuppressWarnings("unchecked")
	private void AllocateMemory(ArrayList<AnyType> array)
	{
		Random generator = new Random( System.nanoTime() );

		if(array == null || array.size() == 0)
		{
			// A completer
			data = null;
			return;
		}
		if(array.size() == 1)
		{
			a = b = 0;
			
			// A completer
			
			data = new QuadraticSpacePerfectHashing[1]; 
			data[0] = new QuadraticSpacePerfectHashing<AnyType>(array);
			return;
		}
		
		
		a = generator.nextInt(p-1)+1;
		b = generator.nextInt(p);
		
		data = new QuadraticSpacePerfectHashing[array.size()];
	
		ArrayList<AnyType> vecteurs[] = new ArrayList[array.size()];
		
		for(int i = 0; i < array.size(); i++)
		{
			if(vecteurs[getKey(array.get(i))] == null)
			{
				vecteurs[getKey(array.get(i))] = new ArrayList<AnyType>();
			}
			
			vecteurs[getKey(array.get(i))].add(array.get(i));
		}
		for(int i = 0; i < array.size(); i++)
		{
			if(vecteurs[i] != null)
			{
				data[i] = new QuadraticSpacePerfectHashing<AnyType>(vecteurs[i]);
			}
		}
		
		return;
	
		// A completer
	}

	public int Size()
	{
		if( data == null ) return 0;

		int size = 0;
		for(int i=0; i<data.length; ++i)
		{
			size += (data[i] == null ? 1 : data[i].Size());
		}
		return size;
	}

	public boolean containsKey(int key)
	{
		// A completer
		if(Size() != 0)
		{
			if(data[key] != null)
			{
				return true;
			}
		}
		return false;
	}
	
	public int getKey (AnyType x) {
		// A completer
		return (((a*x.hashCode()+b)%p)%data.length);
	}
	
	public boolean containsValue (AnyType x) {
		// A completer
		if (Size() != 0)
		{
			if(data[getKey(x)] != null && data[getKey(x)].containsValue(x))
			{
				return true;
			}
			return false;
		}
		return false;
	
	}
	
	public void remove (AnyType x) {
		// A completer
		if (containsValue(x))
		{
			data[getKey(x)].remove(x);
		}
		
	}

	public String toString () {
		String result = "";
		
		// A completer
		for(int i = 0; i < data.length; i++)
		{
			if(data[i] != null)
			{
				result += "[" + i + "] -> " + data[i] + "\n"; 
			}
		}
		return result;
	}

	public void makeEmpty () {
		// A completer
		data = null;

   	}
	
}
