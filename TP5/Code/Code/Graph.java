import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class Graph {

	private List<Node> nodes = new ArrayList<Node>(); // Noeuds
	private List<Edge> edges = new ArrayList<Edge>();; // Les arcs
	static final double inf = 99999;

	public Graph() {
		
	}
	
	public double toDouble(String s)
	{
		return Double.parseDouble(s);
	}

	public void readFromFile(String filePath,String separtor){
		//compl�ter

		try {

			BufferedReader in = new BufferedReader(new FileReader(filePath));

			String lu;
			String premiereLigne = in.readLine();
			
			String[] element = premiereLigne.split(separtor);
			
			for(int i = 0; i < element.length; i++)
			{
				nodes.add(new Node(i,element[i]));
			}

			while ((lu = in.readLine()) != null) {
				String[] distance = lu.split(separtor);
				int source = 0;
				
					for(int i = 0; i < distance.length; i++)
					{
						if(toDouble(distance[i]) == 0)
						{
							source = i;
							break;
						}
					}
					
					for(int i = 0; i < distance.length; i++)
					{
						Edge e = new Edge(nodes.get(source), nodes.get(i), toDouble(distance[i]));
						
						if(toDouble(distance[i]) != 0) {
							edges.add(e);
						}
						else if(distance[i].equals("inf"))
						{
							e.setDsitance(inf);
							edges.add(e);
						}
					}
					
				} in.close();
			
			for(int i = 0; i < nodes.size();i++)
			{
				System.out.println(nodes.get(i).getName() + ',');
			}
			for(int i = 0; i < edges.size(); i++)
			{
				for(int j = 0; j < nodes.size(); j++)
				{
					System.out.println(edges.get(i).getDistance() + ',');
				}
				System.out.println("\n");
			}
			
			}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	
	
	/*public List<Edge> getOutEdges(Node source) {
		List<Edge> outEdges ; 
		// compl�ter

		return outEdges;	
	}
	
	public List<Edge> getInEdges(Node dest) {
		List<Edge> inEdges ; 
		// compl�ter
		return inEdges;		
	}*/
	// Accesseurs 
	public List<Node> getNodes() {
		return nodes;
	}
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
	public List<Edge> getEdges() {
		return edges;
	}
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
	public Node getNodeByName(String name){
		for (Node node : nodes) {
			if(node.getName().equals(name)){
				return node;
			}
		}
		return null;
	}
	
	public Node getNodeById(int id){
		for (Node node : nodes) {
			if(node.getId()==id){
				return node;
			}

		}
		return null;
	}
}
