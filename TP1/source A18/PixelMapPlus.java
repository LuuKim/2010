import java.awt.PageAttributes.ColorType;

import PixelMap.ImageType;

/**
 * Classe PixelMapPlus
 * Image de type noir et blanc, tons de gris ou couleurs
 * Peut lire et ecrire des fichiers PNM
 * Implemente les methodes de ImageOperations
 * @author : 
 * @date   : 
 */

public class PixelMapPlus extends PixelMap implements ImageOperations 
{
	/**
	 * Constructeur creant l'image a partir d'un fichier
	 * @param fileName : Nom du fichier image
	 */
	PixelMapPlus(String fileName)
	{
		super( fileName );
	}
	
	/**
	 * Constructeur copie
	 * @param type : type de l'image a creer (BW/Gray/Color)
	 * @param image : source
	 */
	PixelMapPlus(PixelMap image)
	{
		super(image); 
	}
	
	/**
	 * Constructeur copie (sert a changer de format)
	 * @param type : type de l'image a creer (BW/Gray/Color)
	 * @param image : source
	 */
	PixelMapPlus(ImageType type, PixelMap image)
	{
		super(type, image); 
	}
	
	/**
	 * Constructeur servant a allouer la memoire de l'image
	 * @param type : type d'image (BW/Gray/Color)
	 * @param h : hauteur (height) de l'image 
	 * @param w : largeur (width) de l'image
	 */
	PixelMapPlus(ImageType type, int h, int w)
	{
		super(type, h, w);
	}
	
	/**
	 * Genere le negatif d'une image
	 */
	public void negate()
	{
		// compl�ter
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				imageData[i][j] = imageData[i][j].Negative();
			}
		}
		
	}
	
	/**
	 * Convertit l'image vers une image en noir et blanc
	 */
	public void convertToBWImage()
	{
		// compl�ter
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				imageData[i][j] = imageData[i][j].toBWPixel();
			}
		}

	}
		
	
	/**
	 * Convertit l'image vers un format de tons de gris
	 */
	public void convertToGrayImage()
	{
		// compl�ter
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				imageData[i][j] = imageData[i][j].toGrayPixel();
			}
		}

	}
		

	
	/**
	 * Convertit l'image vers une image en couleurs
	 */
	public void convertToColorImage()
	{
		// compl�ter
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
			imageData[i][j] = imageData[i][j].toColorPixel();
			}
		}
		
	}
	
	public void convertToTransparentImage()
	{
		// compl�ter
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				imageData[i][j] = imageData[i][j].toTransparentPixel();
			}
		}
	}
	
	
	/**
	 * Modifie la longueur et la largeur de l'image 
	 * @param w : nouvelle largeur
	 * @param h : nouvelle hauteur
	 */
	public void resize(int w, int h) throws IllegalArgumentException
	
	{
		if(w < 0 || h < 0)
			throw new IllegalArgumentException();
		
		// compl�ter

		double nouvelleHauteur = ((double)height)/h;
		double nouvelleLargeur = ((double)width)/w;
		PixelMap copie = new PixelMap(this);
		AllocateMemory(imageType,h,w);
		
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{

				imageData[i][j] = copie.getPixel((int)(nouvelleHauteur)*i,(int)(nouvelleLargeur)*j);
				
			}
		}
		width = w;
		height = h;
		
	}
	
	/**
	 * Insert pm dans l'image a la position row0 col0
	 */
	public void insert(PixelMap pm, int row0, int col0)
	{
		// compl�ter
		int largeur_pm = pm.getWidth();
		int hauteur_pm = pm.getHeight();
		
		if(row0 <= height && col0 <= width)
		{
			for (int i = 0; i + row0 < height && i < hauteur_pm; i++)
			{
				for (int j = 0; j + col0 < width && j < largeur_pm; j++)
				{
					imageData[i + row0][j + col0] = pm.getPixel(i,j);
				}
			}
		}	
	}
	
	/**
	 * Decoupe l'image 
	 */
	public void crop(int h, int w)
	{
		// compl�ter
		
		AbstractPixel [][] copie = new AbstractPixel[h][w];
		for (int i = 0; i < h; i++)
		{
			for (int j = 0; j < w; j++)
			{
				if (i< height && j < width)
				{
				copie[i][j] = imageData[i][j];
				}
				else 
					copie[i][j] = new BWPixel(true);
			}
		width = w;
		height = h;
		imageData = copie;
		
		}
	}
		
	
	
	/**
	 * Effectue une translation de l'image 
	 */
	public void translate(int rowOffset, int colOffset)
	{
		// compl�ter	
		PixelMap copie = new PixelMap(this);
		
		for(int i = 0; i < height; i++ ) 
		{
			for (int j = 0; i < width ; i++)
			{
				if(rowOffset <= height && colOffset <= width &&
						rowOffset >= 0 && colOffset >= 0)
				{
					imageData[i + rowOffset][j + colOffset] = copie.getPixel(i,j);
				}
				else
				{
					imageData[i][j] = new BWPixel(true);
				}

			}
		}		
		imageData = copie.imageData;
	}
}
					
			
		
	
	

